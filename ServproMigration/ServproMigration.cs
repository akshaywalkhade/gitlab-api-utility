﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServproMigration
{
    public partial class ServproMigration : Form
    {
        HttpClient sourceClient;
        HttpClient targetClient;
        public ServproMigration()
        {
            InitializeComponent();
        }

        private void ServproMigration_Load(object sender, EventArgs e)
        {

        }

        private void Migrate_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = " ";
                lblSuccess.Text = " ";
                string sourceResponseString = string.Empty;
                StringContent content;
                Dictionary<string, dynamic> postData;
                string jsonObject = string.Empty;
                string entity = cbEnity.SelectedItem.ToString();
                string sourceUrl = txtSourceApi.Text + "?per_page=100";
                string targetUrl = txtTargetApi.Text;
                string token = txtToken.Text;

                using (var sourceClient = new HttpClient())
                {
                    sourceClient.BaseAddress = new Uri(sourceUrl);
                    sourceClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", txtToken.Text);
                    sourceClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage sourceResponse = Task.FromResult(sourceClient.GetAsync(sourceUrl)).Result.Result;

                        if (!sourceResponse.IsSuccessStatusCode)
                        {
                            lblError.Text = string.Format("Source API response error. Response Status Code: {0}", sourceResponse.StatusCode.ToString());
                            return;
                        }

                        sourceResponseString = sourceResponse.Content.ReadAsStringAsync().Result;
                        var sourceList = JsonConvert.DeserializeObject<List<dynamic>>(sourceResponseString);

                        foreach (var obj in sourceList)
                        {
                            using (targetClient = new HttpClient())
                            {
                                switch (entity)
                                {
                                    case "environments":
                                        postData = new Dictionary<string, dynamic>();
                                        postData.Add("name", obj.name.Value);
                                        postData.Add("external_url", obj.external_url.Value);

                                        jsonObject = JsonConvert.SerializeObject(postData);
                                        break;
                                    case "project variables":
                                    case "group variables":
                                        //{
                                        //    "key": "NEW_VARIABLE",
                                        //    "value": "new value",
                                        //    "protected": false,
                                        //    "variable_type": "env_var",
                                        //    "masked": false,
                                        //    "environment_scope": "*"
                                        //}

                                        postData = new Dictionary<string, dynamic>();
                                        postData.Add("key", obj.key.Value);
                                        postData.Add("value", obj.value.Value);
                                        postData.Add("protected", false);
                                        postData.Add("variable_type", obj.variable_type.Value);
                                        postData.Add("masked", obj.masked.Value);
                                        if (entity == "project variables")
                                            postData.Add("environment_scope", obj.environment_scope.Value);

                                        jsonObject = JsonConvert.SerializeObject(postData);
                                        break;
                                }

                                content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
                                targetClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", txtToken.Text);
                                targetClient.DefaultRequestHeaders.Accept.Add(
                                new MediaTypeWithQualityHeaderValue("application/json"));

                                try
                                {
                                    var targetResponse = targetClient.PostAsync(targetUrl, content).Result;
                                    if (!targetResponse.IsSuccessStatusCode)
                                    {
                                        throw new Exception(string.Format("\nTarget API response error. Response Status Code: {0}", targetResponse.StatusCode.ToString()));
                                    }

                                    string apiResponse = targetResponse.Content.ReadAsStringAsync().Result;
                                    var insertedItem = JsonConvert.DeserializeObject<dynamic>(apiResponse);
                                }
                                catch (Exception ex)
                                {
                                    lblError.Text += string.Format("\nError pushing data to Target. Error: {0}", ex.Message);
                                    continue;
                                }
                            }
                        }

                        lblSuccess.Text = string.Format("{0} inserted successfully", entity);
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = string.Format("Error fetching data using source API. Error: {0}", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}
