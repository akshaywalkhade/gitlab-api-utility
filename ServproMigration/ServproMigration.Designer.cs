﻿namespace ServproMigration
{
    partial class ServproMigration
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSourceApi = new System.Windows.Forms.TextBox();
            this.txtTargetApi = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.Migrate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSuccess = new System.Windows.Forms.Label();
            this.cbEnity = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Source API";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Target API";
            // 
            // txtSourceApi
            // 
            this.txtSourceApi.Location = new System.Drawing.Point(95, 95);
            this.txtSourceApi.Name = "txtSourceApi";
            this.txtSourceApi.Size = new System.Drawing.Size(645, 23);
            this.txtSourceApi.TabIndex = 3;
            this.txtSourceApi.Text = "https://gitlab.com/api/v4/projects/17978395/variables";
            // 
            // txtTargetApi
            // 
            this.txtTargetApi.Location = new System.Drawing.Point(95, 135);
            this.txtTargetApi.Name = "txtTargetApi";
            this.txtTargetApi.Size = new System.Drawing.Size(645, 23);
            this.txtTargetApi.TabIndex = 3;
            this.txtTargetApi.Text = "https://gitlab.com/api/v4/projects/19033240/variables";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Token";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(95, 177);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(302, 23);
            this.txtToken.TabIndex = 4;
            this.txtToken.Text = "9JP8-Kw3di4aZcydppKF";
            // 
            // Migrate
            // 
            this.Migrate.Location = new System.Drawing.Point(95, 230);
            this.Migrate.Name = "Migrate";
            this.Migrate.Size = new System.Drawing.Size(75, 23);
            this.Migrate.TabIndex = 5;
            this.Migrate.Text = "Migrate";
            this.Migrate.UseVisualStyleBackColor = true;
            this.Migrate.Click += new System.EventHandler(this.Migrate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(95, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 2;
            // 
            // lblSuccess
            // 
            this.lblSuccess.AutoSize = true;
            this.lblSuccess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblSuccess.Location = new System.Drawing.Point(95, 9);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.Size = new System.Drawing.Size(10, 15);
            this.lblSuccess.TabIndex = 2;
            this.lblSuccess.Text = " ";
            // 
            // cbEnity
            // 
            this.cbEnity.FormattingEnabled = true;
            this.cbEnity.Items.AddRange(new object[] {
            "environments",
            "project variables",
            "group variables"});
            this.cbEnity.Location = new System.Drawing.Point(95, 53);
            this.cbEnity.Name = "cbEnity";
            this.cbEnity.Size = new System.Drawing.Size(121, 23);
            this.cbEnity.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Entity";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(95, 289);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(40, 15);
            this.lblError.TabIndex = 7;
            this.lblError.Text = "Errors:";
            // 
            // ServproMigration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 654);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbEnity);
            this.Controls.Add(this.lblSuccess);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Migrate);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTargetApi);
            this.Controls.Add(this.txtSourceApi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "ServproMigration";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ServproMigration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSourceApi;
        private System.Windows.Forms.TextBox txtTargetApi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Button Migrate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSuccess;
        private System.Windows.Forms.ComboBox cbEnity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblError;
    }
}

